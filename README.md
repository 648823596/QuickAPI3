# QuickAPI3

QuickAPI3是一个生成后台API接口并且支持在线测试的Web服务框架.

> 目前版本只支持SpringBoot环境

> 使用前请确保JAVA_HOME环境变量已经设置

# 快速入门

## 1 引入QuickAPI
```xml
<dependency>
      <groupId>cn.schoolwow</groupId>
      <artifactId>QuickAPI</artifactId>
      <version>{最新版本}</version>
    </dependency>
```

> [QuickAPI最新版本查询](https://search.maven.org/search?q=a:QuickAPI)

## 2 启用QuickAPI

注册一个类型为QuickAPIOption的Bean即可

```java
@Configuration
public class QuickAPITestConfig {
    @Bean
    public QuickAPIOption quickAPIOption(){
        QuickAPIOption quickAPIOption = new QuickAPIOption();
        quickAPIOption.debug = true;
        quickAPIOption.sourceJavaProjectPath = System.getProperty("user.dir")+"/src/test/java";
        quickAPIOption.controllerPackageNameList = Arrays.asList(
                "cn.schoolwow.quickapi.test"
        );
        quickAPIOption.entityPackageNameList = Arrays.asList(
                "cn.schoolwow.quickapi.test.module",
                "cn.schoolwow.quickdao.domain"
        );
        quickAPIOption.apiFieldListener = new APIFieldListener() {
            @Override
            public void handleAPIEntity(APIField apiField) {
                Comment comment = apiField.field.getAnnotation(Comment.class);
                if(null!=comment){
                    apiField.description = comment.value();
                }
            }
        };
        return quickAPIOption;
    }
}

```

## 3 访问QuickAPI地址

启动程序,访问http://ip:port/quickapi/index.html即可

# 详细文档

[点此访问](https://quickapi.schoolwow.cn/)

# 反馈

* 提交Issue
* 邮箱: 648823596@qq.com

# 开源协议
本软件使用 [GPL](http://www.gnu.org/licenses/gpl-3.0.html) 开源协议!