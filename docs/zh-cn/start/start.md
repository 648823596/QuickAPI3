# 快速入门

## 1 引入QuickAPI
```xml
<dependency>
      <groupId>cn.schoolwow</groupId>
      <artifactId>QuickAPI</artifactId>
      <version>{最新版本}</version>
    </dependency>
```

> [QuickAPI最新版本查询](https://search.maven.org/search?q=a:QuickAPI)

## 2 启用QuickAPI

引用依赖后自动启用QuickAPI,您只需要访问http://ip:port/quickapi/index.html即可

# 进阶配置

您可以通过设置API配置对象来改变默认的API生成过程.[点此访问](/zh-cn/start/option.md)