ams.block("executeHttpRequestFormBlock",{
    "resource":{
        "fields":{
            "request": {
                "type": "object",
                "label": "HTTP请求",
                "fields": {
                    "method":{
                        "type": "select",
                        "label": "请求方法",
                        "props": {
                            "placeholder": "请选择",
                            "clearable": true,
                            "multiple": false,
                            "options": {
                                "GET": "GET",
                                "POST": "POST",
                                "PUT": "PUT",
                                "DELETE": "DELETE",
                            },
                        },
                        "default": "GET",
                        "rules": [{
                            "required": true,
                            "message": "请选择请求方法",
                            "trigger": "blur"
                        }],
                    },
                    "url":{
                        "type": "text",
                        "label": "请求地址",
                        "props":{
                            "placeholder": "请输入请求地址"
                        },
                        "default": "/",
                        "rules": [{
                            "required": true,
                            "message": "请输入请求地址",
                            "trigger": "blur"
                        }],
                    },
                    "contentType":{
                        "type": "select",
                        "label": "请求类型",
                        "props": {
                            "placeholder": "请选择",
                            "clearable": true,
                            "multiple": false,
                            "options": {
                                "form": "表单",
                                "json": "JSON",
                            },
                        },
                        "default": "form",
                        "rules": [{
                            "required": true,
                            "message": "请输入请求类型",
                            "trigger": "blur"
                        }],
                    },
                    "headerMap":{
                        "type": "textarea",
                        "label": "请求头部",
                        "props":{
                            "placeholder": "请输入请求头部"
                        },
                    },
                    "body":{
                        "type": "textarea",
                        "label": "请求参数",
                        "props":{
                            "placeholder": "请输入请求参数"
                        },
                    },
                }
            },
            "response": {
                "type": "object",
                "label": "HTTP响应",
                "fields": {
                    "status":{
                        "type": "text",
                        "label": "状态码",
                        "props":{
                            "disabled":true
                        }
                    },
                    "header":{
                        "type": "textarea",
                        "label": "返回头部",
                        "props":{
                            "disabled":true
                        }
                    },
                    "body":{
                        "type": "textarea",
                        "label": "返回体",
                        "props":{
                            "disabled":true
                        }
                    },
                }
            }
        },
    },
    "type": "form",
    "ctx": "edit",
    "events": {
        "init": "initialForm"
    },
    "operations": {
        "submit": {
            "type": "button",
            "label": "执行请求",
            "props": {
                "type": "primary",
            },
            "event": "validate execute"
        },
    },
    "actions":{
        initialForm(){
            let executeHttpRequest = localStorage.getItem("executeHttpRequest");
            if(null!=executeHttpRequest){
                this["data"]["request"] = JSON.parse(executeHttpRequest);
            }
        },
        execute(){
            let data = this.data;
            let headerMap = JSON.parse(data["request"]["headerMap"]);
            let body = data["request"]["body"];
            if("json"===data["request"]["contentType"]){
                try {
                    body = JSON.parse(body);
                }catch (e) {}
            }
            this.showLoading();
            return ams.request({
                "method": data["request"]["method"],
                "url": data["request"]["url"],
                "headers": headerMap,
                "data": body,
                "withCredentials": true,
                "contentType": data["request"]["contentType"],
            }).then((response)=>{
                this.$set(this["data"]["response"], "status", response["status"]);
                if(response.hasOwnProperty("content")){
                    this.$set(this["data"]["response"], "body", response["content"]);
                }else{
                    this.$set(this["data"]["response"], "body", JSON.stringify(response["data"]));
                }
            }).finally(()=>{
                this.hideLoading();
            });
        }
    }
});