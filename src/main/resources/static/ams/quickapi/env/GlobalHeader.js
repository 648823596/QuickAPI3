userBlockMap["quickapi|clazz|GlobalHeader"] = {
    "events": {
        "list": "@list saveToLocal"
    },
    "actions":{
        saveToLocal(params){
            let o = params.$prevReturn;
            let list = o["data"]["data"]["list"];
            localStorage.setItem("globalHeader", JSON.stringify(list));
        }
    },
};