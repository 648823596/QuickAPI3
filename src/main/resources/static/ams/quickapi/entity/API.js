userBlockMap["quickapi|clazz|API"] = {
    "operations": {
        "executeHttpRequest": {
            "type": "button",
            "label": "执行请求",
            "props": {
                "type": "primary",
            },
            "event": "executeHttpRequest"
        },
    },
    "actions":{
        executeHttpRequest(params){
            let row = params.$prevReturn;
            let contentType = row["contentType"];
            if(null!=contentType&&contentType!==""){
                switch (row["contentType"]) {
                    case "application/x-www-form-urlencoded":{contentType = "form";}break;
                    case "application/json":{contentType = "json";}break;
                    default:{
                        this.$message.error("不支持执行该类型请求!类型:"+contentType);
                    }
                }
            }else{
                contentType = "form";
            }

            let headerMap = row["headerMap"];
            try {
                let list = JSON.parse(localStorage.getItem("globalHeader"));
                for(let i=0;i<list.length;i++){
                    let o = list[i];
                    if(headerMap.hasOwnProperty(o["headerName"])){
                        headerMap["headerMap"] = o["headerValue"];
                    }
                }
            }catch (e) {

            }

            let requestBody = null==row["requestBody"]?null:JSON.stringify(row["requestBody"]);
            let query = {
                "method": row["requestMethod"],
                "url": row["url"],
                "contentType": contentType,
                "headerMap": JSON.stringify(headerMap),
                "body": requestBody
            };
            localStorage.setItem("executeHttpRequest", JSON.stringify(query));
            this.callAction("routerPush", {
                "path": "/execute/quickapi|form|executeHttpRequest",
            });
        }
    }
};