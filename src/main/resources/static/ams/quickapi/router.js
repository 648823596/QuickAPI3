routes.push(
    {
        "path": "/env",
        "name": "环境配置",
        "children":[
            {
                "path": "/quickapi|clazz|GlobalHeader",
                "name": "全局头部",
            },
        ]
    },
    // {
    //     "path": "/api",
    //     "name": "项目接口",
    //     "children":[
    //         {
    //             "path": "/quickapi|clazz|APIController",
    //             "name": "控制器列表",
    //         },
    //         {
    //             "path": "/quickapi|clazz|API",
    //             "name": "接口列表",
    //         },
    //         {
    //             "path": "/quickapi|clazz|APIParameter",
    //             "name": "接口参数列表",
    //         },
    //     ]
    // },
    // {
    //     "path": "/entity",
    //     "name": "实体类",
    //     "children":[
    //         {
    //             "path": "/quickapi|clazz|ParameterEntity",
    //             "name": "请求参数实体类列表",
    //         },
    //         {
    //             "path": "/quickapi|clazz|ReturnEntity",
    //             "name": "返回参数实体类列表",
    //         },
    //         {
    //             "path": "/quickapi|clazz|APIField",
    //             "name": "实体类字段列表",
    //         },
    //     ]
    // },
    {
        "path": "/api",
        "name": "接口",
        "children":[
            {
                "path": "/quickapi|clazz|API",
                "name": "接口列表",
            },
        ]
    },
    {
        "path": "/execute",
        "name": "执行",
        "children":[
            {
                "path": "/quickapi|form|executeHttpRequest",
                "block": "executeHttpRequestFormBlock",
                "name": "执行请求",
            },
        ]
    },
);