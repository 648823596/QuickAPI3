package cn.schoolwow.quickapi.domain;

import cn.schoolwow.quickapi.module.business.api.domain.API;

public interface APIListener {
    /**
     * 处理api
     * */
    void handleAPI(API api);
}
