package cn.schoolwow.quickapi.domain;

import cn.schoolwow.quickapi.module.common.domain.APIField;

public interface APIFieldListener {
    /**
     * 处理api字段
     * */
    void handleAPIEntity(APIField apiField);
}
