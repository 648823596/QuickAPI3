package cn.schoolwow.quickapi.domain;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * QuickAPI配置项
 * */
public class QuickAPIOption {
    /**是否开启调试模式*/
    public boolean debug;

    /**Java源文件项目路径*/
    public String sourceJavaProjectPath = System.getProperty("user.dir")+"/src/main/java";

    /**指定待扫描控制器包名*/
    public List<String> controllerPackageNameList = new ArrayList<>();

    /**实体类包名*/
    public List<String> entityPackageNameList;

    /**APIController监听*/
    public APIControllerListener apiControllerListener;

    /**API监听*/
    public APIListener apiListener;

    /**APIParameter监听*/
    public APIParameterListener apiParameterListener;

    /**APIEntity监听*/
    public APIEntityListener apiEntityListener;

    /**APIField监听*/
    public APIFieldListener apiFieldListener;

    /**全局头部*/
    public JSONObject headerMap = new JSONObject();

}