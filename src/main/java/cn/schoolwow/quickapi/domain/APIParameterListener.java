package cn.schoolwow.quickapi.domain;

import cn.schoolwow.quickapi.module.business.apiParameter.domain.APIParameter;

public interface APIParameterListener {
    /**
     * 处理api参数
     * */
    void handleAPIParameter(APIParameter apiParameter);
}
