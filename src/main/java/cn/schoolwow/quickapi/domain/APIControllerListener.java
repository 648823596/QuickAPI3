package cn.schoolwow.quickapi.domain;

import cn.schoolwow.quickapi.module.business.apiController.domain.APIController;

public interface APIControllerListener {
    /**
     * 处理APIController
     * */
    void handleAPIController(APIController apiController);
}
