package cn.schoolwow.quickapi.domain;

import cn.schoolwow.quickapi.module.common.domain.APIEntity;

public interface APIEntityListener {
    /**
     * 处理api实体类
     * */
    void handleAPIEntity(APIEntity apiEntity);
}
