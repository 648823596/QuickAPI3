package cn.schoolwow.quickapi.module.initial.flow;

import cn.schoolwow.quickapi.domain.QuickAPIOption;
import cn.schoolwow.quickflow.domain.FlowContext;
import cn.schoolwow.quickflow.flow.BusinessFlow;
import cn.schoolwow.util.domain.check.instance.CheckInstance;

public class CheckQuickAPIOptionFlow implements BusinessFlow {
    @Override
    public void executeBusinessFlow(FlowContext flowContext) throws Exception {
        QuickAPIOption quickAPIOption = flowContext.checkInstanceData(QuickAPIOption.class);

        if(null!=quickAPIOption.entityPackageNameList&&!quickAPIOption.entityPackageNameList.isEmpty()){
            for(String entityPackageName:quickAPIOption.entityPackageNameList){
                if(!entityPackageName.contains(".")){
                    throw new IllegalArgumentException("实体类包名格式不合法!实体类包名:"+entityPackageName);
                }
            }
        }
        CheckInstance.newCheck()
                .notEmptyCheck(quickAPIOption.controllerPackageNameList, "控制器包名列表不能为空");
        for(String controllerPackageName:quickAPIOption.controllerPackageNameList){
            if(!controllerPackageName.contains(".")){
                throw new IllegalArgumentException("控制器类包名格式不合法!控制器类包名:"+controllerPackageName);
            }
        }
    }

    @Override
    public String name() {
        return "检查用户初始化参数";
    }
}
