package cn.schoolwow.quickapi.module.initial.service;

import cn.schoolwow.quickapi.module.business.api.flow.SetRequestAndResponsePropertyFlow;
import cn.schoolwow.quickapi.module.business.api.service.SetAPIListFlow;
import cn.schoolwow.quickapi.module.business.apiController.service.SetAPIControllerListFlow;
import cn.schoolwow.quickapi.module.business.apiParameter.service.SetAPIParameterListFlow;
import cn.schoolwow.quickapi.module.business.apidoc.flow.SetJavaProjectBuilderFlow;
import cn.schoolwow.quickapi.module.business.apidoc.service.SetAPICommentFlow;
import cn.schoolwow.quickapi.module.initial.flow.ExecuteQuickAPIListenerFlow;
import cn.schoolwow.quickapi.module.initial.flow.SetAPIParameterAndReturnDataFlow;
import cn.schoolwow.quickapi.module.initial.flow.SetInitialFlagFlow;
import cn.schoolwow.quickflow.QuickFlowExecutor;
import cn.schoolwow.quickflow.flow.CompositeBusinessFlow;

public class InitialQuickAPICompositeBusiness implements CompositeBusinessFlow {
    @Override
    public void executeCompositeBusiness(QuickFlowExecutor quickFlowExecutor) {
        quickFlowExecutor
                .next(new SetJavaProjectBuilderFlow())
                .next(new SetAPIControllerListFlow())
                .next(new SetAPIListFlow())
                .next(new SetAPIParameterListFlow())
                .next(new SetAPICommentFlow())
                .next(new ExecuteQuickAPIListenerFlow())
                .next(new SetAPIParameterAndReturnDataFlow())
                .next(new SetRequestAndResponsePropertyFlow())
                .next(new SetInitialFlagFlow());
    }

    @Override
    public String name() {
        return "初始化QuickAPI流程对象";
    }
}
