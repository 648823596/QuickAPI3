package cn.schoolwow.quickapi.module.initial.flow;

import cn.schoolwow.quickapi.module.business.api.domain.API;
import cn.schoolwow.quickapi.module.common.domain.APIDocument;
import cn.schoolwow.quickapi.module.common.domain.APIEntity;
import cn.schoolwow.quickflow.domain.FlowContext;
import cn.schoolwow.quickflow.flow.BusinessFlow;

public class SetAPIParameterAndReturnDataFlow implements BusinessFlow {
    @Override
    public void executeBusinessFlow(FlowContext flowContext) throws Exception {
        APIDocument apiDocument = flowContext.checkInstanceData(APIDocument.class);

        for(API api:apiDocument.apiList){
            if(!api.parameterEntityNameList.isEmpty()){
                for(String className:api.parameterEntityNameList){
                    APIEntity apiEntity = apiDocument.apiEntityMap.get(className);
                    api.parameterData.put(apiEntity.className, apiEntity);
                }
            }
            if(!api.returnEntityNameList.isEmpty()){
                for(String className:api.returnEntityNameList){
                    APIEntity apiEntity = apiDocument.apiEntityMap.get(className);
                    api.returnData.put(apiEntity.className, apiEntity);
                }
            }
        }
    }

    @Override
    public String name() {
        return "设置API的请求参数数据和返回参数数据";
    }
}
