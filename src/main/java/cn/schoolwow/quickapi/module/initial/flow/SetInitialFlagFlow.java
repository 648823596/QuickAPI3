package cn.schoolwow.quickapi.module.initial.flow;

import cn.schoolwow.quickflow.domain.FlowContext;
import cn.schoolwow.quickflow.flow.BusinessFlow;

public class SetInitialFlagFlow implements BusinessFlow {
    @Override
    public void executeBusinessFlow(FlowContext flowContext) throws Exception {
        flowContext.putContextData("hasInitial", true);
    }

    @Override
    public String name() {
        return "设置初始化标志";
    }
}
