package cn.schoolwow.quickapi.module.ams.header.aware;

import cn.schoolwow.ams.domain.block.list.clazz.ClazzListBlockConfigAware;
import cn.schoolwow.ams.domain.block.list.clazz.ClazzListBlockOption;
import cn.schoolwow.quickapi.module.ams.domain.QuickAPIConstant;
import cn.schoolwow.quickapi.module.ams.header.domain.GlobalHeader;
import org.springframework.stereotype.Service;

@Service
public class GlobalHeaderClazzListBlockAware extends ClazzListBlockConfigAware {
    @Override
    public ClazzListBlockOption getAMSBlockConfig() {
        ClazzListBlockOption clazzListBlockOption = new ClazzListBlockOption(
                QuickAPIConstant.PROJECT_NAME, "全局头部", GlobalHeader.class);
        return clazzListBlockOption;
    }

}
