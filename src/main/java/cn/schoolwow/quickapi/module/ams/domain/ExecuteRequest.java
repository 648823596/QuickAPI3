package cn.schoolwow.quickapi.module.ams.domain;

import cn.schoolwow.ams.domain.annotation.AMSFieldDefaultValue;
import cn.schoolwow.ams.domain.annotation.AMSFieldLabel;
import cn.schoolwow.ams.domain.annotation.AMSFieldProperty;
import cn.schoolwow.ams.domain.annotation.AMSFieldType;

public class ExecuteRequest {
    @AMSFieldLabel("请求方法(GET:GET,POST:POST,PUT:PUT,DELETE:DELETE)")
    @AMSFieldDefaultValue("GET")
    public String requestMethod;

    @AMSFieldLabel("请求url")
    public String url;

    @AMSFieldLabel("请求类型(form:表单,json:JSON)")
    @AMSFieldDefaultValue("json")
    public String contentType;

    @AMSFieldLabel("请求参数")
    @AMSFieldType("textarea")
    public String body;

    @AMSFieldLabel("状态码")
    @AMSFieldProperty(disable = true)
    public String statusCode;

    @AMSFieldLabel("返回头部")
    @AMSFieldProperty(disable = true)
    public String responseHeader;

    @AMSFieldLabel("返回体")
    @AMSFieldProperty(disable = true)
    public String responseBody;

}
