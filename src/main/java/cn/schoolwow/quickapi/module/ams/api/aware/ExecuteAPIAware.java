//package cn.schoolwow.quickapi.module.ams.api.aware;
//
//import cn.schoolwow.ams.domain.block.common.operation.FormBlockHandlerBlockOperation;
//import cn.schoolwow.ams.domain.block.form.FormBlockConfigAware;
//import cn.schoolwow.ams.domain.block.form.FormBlockOperationHandler;
//import cn.schoolwow.ams.domain.block.form.FormBlockOption;
//import cn.schoolwow.ams.domain.block.form.FormBlockRequest;
//import cn.schoolwow.quickapi.module.ams.domain.ExecuteRequest;
//import com.alibaba.fastjson.JSONObject;
//import org.springframework.stereotype.Service;
//
//@Service
//public class ExecuteAPIAware extends FormBlockConfigAware {
//    @Override
//    public FormBlockOption getAMSBlockConfig() {
//        FormBlockOption formBlockOption = new FormBlockOption("test", "执行请求", ExecuteRequest.class);
//        {
//            FormBlockHandlerBlockOperation formBlockHandlerBlockOperation = new FormBlockHandlerBlockOperation("执行请求", new FormBlockOperationHandler() {
//                @Override
//                public JSONObject handle(FormBlockRequest formBlockRequest) throws Exception {
//                    ExecuteRequest executeRequest = formBlockRequest.data.toJavaObject(ExecuteRequest.class);
//                    Request request = QuickHttp.connect(executeRequest.url)
//                            .method(executeRequest.requestMethod)
//                            .requestBody(executeRequest.body);
//                    switch (executeRequest.contentType){
//                        case "form":{
//                            request.contentType(Request.ContentType.APPLICATION_X_WWW_FORM_URLENCODED);
//                        }break;
//                        case "json":{
//                            request.contentType(Request.ContentType.APPLICATION_JSON);
//                        }break;
//                    }
//                    Response response = request.execute();
//
//                    JSONObject result = new JSONObject();
//                    result.put("statusCode", response.statusCode());
//                    result.put("responseHeader", response.headers());
//                    result.put("responseBody", response.body());
//                    return result;
//                }
//            });
//            formBlockOption.formBlockHandlerBlockOperationList.add(formBlockHandlerBlockOperation);
//        }
//        return formBlockOption;
//    }
//}
