package cn.schoolwow.quickapi.module.ams.header.domain;

import cn.schoolwow.ams.domain.annotation.AMSFieldLabel;

public class GlobalHeader {
    @AMSFieldLabel("头部名称")
    public String headerName;

    @AMSFieldLabel("头部值")
    public String headerValue;
}
