package cn.schoolwow.quickapi.module.common.domain;

import cn.schoolwow.quickdao.annotation.Comment;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;

import java.lang.reflect.Field;

@Comment("实体类字段")
public class APIField {
    @Comment("字段名")
    public String name;

    @Comment("描述")
    public String description;

    @Comment("字段类型")
    public String type;

    @Comment("字段泛型类型")
    public String genericTypeClassName;

    @Comment("字段Java类型")
    public transient String classType;

    @Comment("字段")
    public transient Field field;

    @Comment("实体类")
    public transient APIEntity apiEntity;

    public transient JavaClass javaClass;

    public transient JavaField javaField;
}