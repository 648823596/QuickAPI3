package cn.schoolwow.quickapi.module.common.domain;

import cn.schoolwow.quickdao.annotation.Comment;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;

import java.util.List;

@Comment("实体类信息")
public class APIEntity {
    @Comment("简写类名")
    public String simpleName;

    @Comment("类名")
    public String className;

    @Comment("描述")
    public String description;

    @Comment("字段列表")
    public List<APIField> apiFieldList;

    @Comment("实体类")
    public transient Class clazz;

    public transient JavaClass javaClass;

    public transient JavaField javaField;

}