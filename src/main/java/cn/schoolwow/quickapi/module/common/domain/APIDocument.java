package cn.schoolwow.quickapi.module.common.domain;

import cn.schoolwow.quickapi.module.business.api.domain.API;
import cn.schoolwow.quickapi.module.business.apiController.domain.APIController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class APIDocument {
    /**控制器列表*/
    public List<APIController> apiControllerList = new ArrayList<>();

    /**接口列表*/
    public List<API> apiList = new ArrayList<>();

    /**实体类信息*/
    public Map<String, APIEntity> apiEntityMap = new HashMap<>();
}
