package cn.schoolwow.quickapi.module.common.flow;

import cn.schoolwow.quickflow.domain.FlowContext;
import cn.schoolwow.quickflow.flow.BusinessFlow;
import cn.schoolwow.util.domain.query.reflection.QueryReflection;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.List;

public class MockDataFlow implements BusinessFlow {
    private Logger logger = LoggerFactory.getLogger(MockDataFlow.class);

    @Override
    public void executeBusinessFlow(FlowContext flowContext) throws Exception {
        Class clazz = (Class) flowContext.checkData("clazz");
        String methodName = flowContext.checkData("methodName", String.class);
        boolean printException = flowContext.getData("printException", boolean.class, false);

        String className = clazz.getName();
        if(!className.contains(".")){
            return;
        }
        if(className.startsWith("java.")||className.startsWith("sun.")){
            return;
        }

        Object instance = null;
        try {
            instance = clazz.getConstructor().newInstance();
            List<Field> fieldList = QueryReflection.newQuery(clazz)
                    .execute()
                    .getFieldList();
            for(Field field:fieldList){
                Object returnObject = flowContext.startFlow(new GetFieldValueObjectFlow())
                        .putCurrentCompositeFlowData("fieldTypeName", field.getGenericType().getTypeName())
                        .execute()
                        .getData("returnObject");
                field.set(instance, returnObject);
            }
        }catch (Exception e){
            logger.warn("模拟返回类数据失败!返回类名:"+className+",涉及方法:"+methodName+",原因:"+e.getMessage());
            if(printException){
                e.printStackTrace();
            }
        }
        if(null==instance){
            return;
        }
        try {
            String jsonString = JSON.toJSONString(instance);
            flowContext.putReturnData("jsonString", jsonString);
        }catch (Exception e){
            logger.warn("格式化参数失败!原因:"+e.getMessage()+",类名:"+className+",方法名:"+methodName);
        }
    }

    @Override
    public String name() {
        return "模拟数据";
    }

}
