package cn.schoolwow.quickapi.module.business.apiParameter.flow.parameter;

import cn.schoolwow.quickapi.module.business.apiParameter.domain.APIParameter;
import cn.schoolwow.quickflow.domain.FlowContext;
import cn.schoolwow.quickflow.flow.BusinessFlow;
import org.springframework.web.bind.annotation.PathVariable;

import java.lang.reflect.Parameter;

public class GetByPathVariableAnnotationFlow implements BusinessFlow {
    @Override
    public void executeBusinessFlow(FlowContext flowContext) throws Exception {
        Parameter parameter = (Parameter) flowContext.checkData("parameter");
        APIParameter apiParameter = (APIParameter) flowContext.checkData("apiParameter");

        PathVariable pathVariable = parameter.getAnnotation(PathVariable.class);
        if(null==pathVariable){
            return;
        }
        apiParameter.name = pathVariable.value();
        apiParameter.required = pathVariable.required();

        flowContext.brokenCurrentCompositeBusiness("");
    }

    @Override
    public String name() {
        return "通过PathVariable注解获取参数";
    }
}
