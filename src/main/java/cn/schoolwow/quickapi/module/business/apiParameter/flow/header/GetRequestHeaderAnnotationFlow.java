package cn.schoolwow.quickapi.module.business.apiParameter.flow.header;

import cn.schoolwow.quickapi.module.business.api.domain.API;
import cn.schoolwow.quickflow.domain.FlowContext;
import cn.schoolwow.quickflow.flow.BusinessFlow;
import org.springframework.web.bind.annotation.RequestHeader;

import java.lang.reflect.Parameter;

public class GetRequestHeaderAnnotationFlow implements BusinessFlow {
    @Override
    public void executeBusinessFlow(FlowContext flowContext) throws Exception {
        API api = (API) flowContext.checkData("api");
        Parameter parameter = (Parameter) flowContext.checkData("parameter");
        String parameterRawName = (String) flowContext.checkData("parameterRawName");

        RequestHeader requestHeader = parameter.getAnnotation(RequestHeader.class);
        if(null==requestHeader){
            return;
        }
        api.headerMap.put(requestHeader.value(), parameterRawName);

        flowContext.putCurrentCompositeFlowData("finished", true);
        flowContext.brokenCurrentCompositeBusiness("");
    }

    @Override
    public String name() {
        return "读取RequestHeader注解";
    }
}
