package cn.schoolwow.quickapi.module.business.api.domain;

import cn.schoolwow.quickapi.module.business.apiController.domain.APIController;
import cn.schoolwow.quickapi.module.business.apiParameter.domain.APIParameter;
import cn.schoolwow.quickdao.annotation.Comment;
import com.alibaba.fastjson.JSONObject;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Comment("接口")
public class API {
    @Comment("标识id")
    public long id = System.nanoTime();

    @Comment("类型")
    public String type = "公共";

    @Comment("描述")
    public String description;

    @Comment("请求方法")
    public String requestMethod = "POST";

    @Comment("请求地址")
    public String url;

    @Comment("是否被废弃")
    public boolean deprecated;

    @Comment("请求头部")
    public JSONObject headerMap = new JSONObject();

    @Comment("请求类型")
    public String contentType = "/";

    @Comment("请求体")
    public String requestBody;

    @Comment("响应体")
    public String responseBody;

    @Comment("返回值类名")
    public String returnClassName;

    @Comment("关联请求参数实体类")
    public Set<String> parameterEntityNameList = new HashSet<>();

    @Comment("关联返回值实体类")
    public Set<String> returnEntityNameList = new HashSet<>();

    @Comment("参数列表")
    public List<APIParameter> apiParameterList = new ArrayList<>();

    @Comment("请求参数实体类信息")
    public JSONObject parameterData = new JSONObject();

    @Comment("返回参数实体类信息")
    public JSONObject returnData = new JSONObject();

    @Comment("整体报文")
    public String requestAndResponse;

    public transient Method method;

    public transient APIController apiController;

    public transient JavaClass javaClass;

    public transient JavaMethod javaMethod;
}