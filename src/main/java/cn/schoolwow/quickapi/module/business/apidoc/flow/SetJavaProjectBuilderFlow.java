package cn.schoolwow.quickapi.module.business.apidoc.flow;

import cn.schoolwow.quickapi.domain.QuickAPIOption;
import cn.schoolwow.quickflow.domain.FlowContext;
import cn.schoolwow.quickflow.flow.BusinessFlow;
import com.thoughtworks.qdox.JavaProjectBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class SetJavaProjectBuilderFlow implements BusinessFlow {
    private Logger logger = LoggerFactory.getLogger(SetJavaProjectBuilderFlow.class);

    @Override
    public void executeBusinessFlow(FlowContext flowContext) throws Exception {
        QuickAPIOption quickAPIOption = flowContext.checkInstanceData(QuickAPIOption.class);

        if(null==quickAPIOption.sourceJavaProjectPath||quickAPIOption.sourceJavaProjectPath.isEmpty()){
            return;
        }

        File sourceFolder = new File(quickAPIOption.sourceJavaProjectPath);
        if(!sourceFolder.exists()){
            logger.warn("java源文件路径不存在!路径:"+sourceFolder.getAbsolutePath());
            return;
        }
        JavaProjectBuilder javaProjectBuilder = new JavaProjectBuilder();
        javaProjectBuilder.addSourceFolder(sourceFolder);
        flowContext.putInstanceData(javaProjectBuilder, JavaProjectBuilder.class);
    }

    @Override
    public String name() {
        return "设置JavaProjectBuilder对象";
    }

}
