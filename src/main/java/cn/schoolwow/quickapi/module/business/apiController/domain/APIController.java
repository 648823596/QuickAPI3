package cn.schoolwow.quickapi.module.business.apiController.domain;

import cn.schoolwow.quickdao.annotation.Comment;
import com.thoughtworks.qdox.model.JavaClass;

@Comment("控制器类")
public class APIController {
    @Comment("类名")
    public String className;

    @Comment("注释")
    public String description;

    @Comment("前缀")
    public String prefix;

    @Comment("是否废弃")
    public boolean deprecated;

    public transient Class clazz;

    public transient JavaClass javaClass;
}
