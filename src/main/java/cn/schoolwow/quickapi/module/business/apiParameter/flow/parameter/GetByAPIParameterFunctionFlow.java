package cn.schoolwow.quickapi.module.business.apiParameter.flow.parameter;

import cn.schoolwow.quickapi.domain.QuickAPIOption;
import cn.schoolwow.quickapi.module.business.apiParameter.domain.APIParameter;
import cn.schoolwow.quickflow.domain.FlowContext;
import cn.schoolwow.quickflow.flow.BusinessFlow;

public class GetByAPIParameterFunctionFlow implements BusinessFlow {
    @Override
    public void executeBusinessFlow(FlowContext flowContext) throws Exception {
        QuickAPIOption quickAPIOption = flowContext.checkInstanceData(QuickAPIOption.class);
        APIParameter apiParameter = (APIParameter) flowContext.checkData("apiParameter");

        if(null==quickAPIOption.apiParameterListener){
            return;
        }
        quickAPIOption.apiParameterListener.handleAPIParameter(apiParameter);
    }

    @Override
    public String name() {
        return "根据APIParameterFunction配置参数获取参数";
    }
}
