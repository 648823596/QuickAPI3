package cn.schoolwow.quickapi.module.business.apiParameter.domain;

import cn.schoolwow.quickapi.module.business.api.domain.API;
import cn.schoolwow.quickdao.annotation.Comment;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;

@Comment("接口请求参数")
public class APIParameter {
    @Comment("参数名称")
    public String name;

    @Comment("描述")
    public String description;

    @Comment("是否必须")
    public boolean required;

    @Comment("注解")
    public transient Annotation annotation;

    @Comment("参数")
    public transient Parameter parameter;

    @Comment("关联接口对象")
    public transient API api;

    public transient JavaClass javaClass;

    public transient JavaMethod javaMethod;

}
