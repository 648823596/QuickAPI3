package cn.schoolwow.quickapi.module.business.apiController.flow;

import cn.schoolwow.quickapi.module.business.apiController.domain.APIController;
import cn.schoolwow.quickflow.domain.FlowContext;
import cn.schoolwow.quickflow.flow.BusinessFlow;
import org.springframework.web.bind.annotation.RequestMapping;

public class SetControllerPrefixFlow implements BusinessFlow {
    @Override
    public void executeBusinessFlow(FlowContext flowContext) throws Exception {
        Class clazz = (Class) flowContext.checkData("clazz");
        APIController apiController = (APIController) flowContext.checkData("apiController");

        RequestMapping requestMapping = (RequestMapping) clazz.getDeclaredAnnotation(RequestMapping.class);
        if(null==requestMapping){
            return;
        }

        if(requestMapping.value().length>0){
            apiController.prefix = requestMapping.value()[0];
        }else{
            apiController.prefix = apiController.clazz.getSimpleName().toLowerCase();
        }
    }

    @Override
    public String name() {
        return "设置控制器路径前缀";
    }
}
