package cn.schoolwow.quickapi.module.business.apiParameter.flow.parameter;

import cn.schoolwow.quickapi.module.business.api.domain.API;
import cn.schoolwow.quickapi.module.business.apiParameter.domain.APIParameter;
import cn.schoolwow.quickapi.module.common.flow.MockDataFlow;
import cn.schoolwow.quickflow.domain.FlowContext;
import cn.schoolwow.quickflow.flow.BusinessFlow;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Parameter;

public class GetByRequestPartAnnotationFlow implements BusinessFlow {
    @Override
    public void executeBusinessFlow(FlowContext flowContext) throws Exception {
        API api = flowContext.checkData("api", API.class);
        Parameter parameter = flowContext.checkData("parameter", Parameter.class);
        APIParameter apiParameter = flowContext.checkData("apiParameter", APIParameter.class);
        StringBuilder requestBodyBuilder = flowContext.checkData("requestBodyBuilder", StringBuilder.class);

        RequestPart requestPart = parameter.getAnnotation(RequestPart.class);
        if(null==requestPart){
            return;
        }
        String value = requestPart.name();
        if(StringUtils.isBlank(value)){
            value = apiParameter.name;
        }
        apiParameter.required = requestPart.required();

        if(MultipartFile.class.getName().equals(parameter.getType().getName())){
            requestBodyBuilder.append("------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n");
            requestBodyBuilder.append("Content-Disposition: form-data; name=\""+value+"\"; filename=\"[原始文件名]\"\r\n");
            requestBodyBuilder.append("\r\n");
            requestBodyBuilder.append("[二进制文件数据]\r\n");
        }else{
            flowContext.startFlow(new MockDataFlow())
                    .putTemporaryData("clazz", parameter.getType())
                    .putTemporaryData("methodName", api.method.getName())
                    .execute();
            String jsonString = flowContext.getData("jsonString", String.class);
            if(null!=jsonString){
                if(jsonString.startsWith("{")){
                    jsonString = JSON.toJSONString(JSON.parseObject(jsonString), SerializerFeature.PrettyFormat);
                }
                if(jsonString.startsWith("[")){
                    jsonString = JSON.toJSONString(JSON.parseArray(jsonString),SerializerFeature.PrettyFormat);
                }
            }else{
                jsonString = value;
            }
            requestBodyBuilder.append("------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n");
            requestBodyBuilder.append("Content-Disposition: form-data; name=\""+value+"\"\r\n");
            requestBodyBuilder.append("\r\n");
            requestBodyBuilder.append(jsonString+"\r\n");
        }
        api.contentType = "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW";

        flowContext.brokenCurrentCompositeBusiness("");
    }

    @Override
    public String name() {
        return "通过RequestPart注解获取参数";
    }
}
