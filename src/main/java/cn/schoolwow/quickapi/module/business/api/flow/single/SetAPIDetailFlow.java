package cn.schoolwow.quickapi.module.business.api.flow.single;

import cn.schoolwow.quickapi.module.business.api.domain.API;
import cn.schoolwow.quickapi.module.business.apiController.domain.APIController;
import cn.schoolwow.quickflow.domain.FlowContext;
import cn.schoolwow.quickflow.flow.BusinessFlow;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;

public class SetAPIDetailFlow implements BusinessFlow {
    @Override
    public void executeBusinessFlow(FlowContext flowContext) throws Exception {
        APIController apiController = (APIController) flowContext.checkData("apiController");
        API api = (API) flowContext.checkData("api");
        Method method = (Method) flowContext.checkData("method");

        if(StringUtils.isBlank(api.url)){
            throw new IllegalArgumentException("url获取失败!方法名:"+api.method.getName()+",类名:"+api.method.getDeclaringClass().getName());
        }
        if (api.url.charAt(0) != '/') {
            api.url = "/" + api.url;
        }
        api.url = apiController.prefix + api.url;
        if(null!=api.method.getAnnotation(Deprecated.class)){
            api.deprecated = true;
        }

        api.method = method;
        api.apiController = apiController;
    }

    @Override
    public String name() {
        return "设置API详细信息";
    }
}
