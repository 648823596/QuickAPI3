package cn.schoolwow.quickapi.module.business.apiController.flow;

import cn.schoolwow.quickapi.module.business.apiController.domain.APIController;
import cn.schoolwow.quickflow.domain.FlowContext;
import cn.schoolwow.quickflow.flow.BusinessFlow;

public class SetControllerDeprecatedFlow implements BusinessFlow {
    @Override
    public void executeBusinessFlow(FlowContext flowContext) throws Exception {
        Class clazz = (Class) flowContext.checkData("clazz");
        APIController apiController = (APIController) flowContext.checkData("apiController");

        apiController.clazz = clazz;
        if(null!=apiController.clazz.getAnnotation(Deprecated.class)){
            apiController.deprecated = true;
        }
    }

    @Override
    public String name() {
        return "设置控制器是否弃用";
    }
}
