package cn.schoolwow.quickapi.util.domain;

import java.io.File;
import java.io.IOException;

public interface SingleFileVisitor {
    /**遍历文件*/
    void visit(File file, File baseDirectory) throws IOException, ClassNotFoundException;
}
