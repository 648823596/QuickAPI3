package cn.schoolwow.quickapi.util.domain;

import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public interface JarEntryVisitor {
    /**遍历文件*/
    void visit(JarURLConnection jarURLConnection, JarFile jarFile, JarEntry jarEntry, URL url) throws IOException, ClassNotFoundException;
}
