package cn.schoolwow.quickapi.test.config;

import cn.schoolwow.quickapi.domain.APIFieldListener;
import cn.schoolwow.quickapi.domain.QuickAPIOption;
import cn.schoolwow.quickapi.module.common.domain.APIField;
import cn.schoolwow.quickdao.annotation.Comment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class QuickAPITestConfig {
    @Bean
    public QuickAPIOption quickAPIOption(){
        QuickAPIOption quickAPIOption = new QuickAPIOption();
        quickAPIOption.debug = true;
        quickAPIOption.sourceJavaProjectPath = System.getProperty("user.dir")+"/src/test/java";
        quickAPIOption.controllerPackageNameList = Arrays.asList(
                "cn.schoolwow.quickapi.test"
        );
        quickAPIOption.entityPackageNameList = Arrays.asList(
                "cn.schoolwow.quickapi.test.module",
                "cn.schoolwow.quickdao.domain"
        );
        quickAPIOption.apiFieldListener = new APIFieldListener() {
            @Override
            public void handleAPIEntity(APIField apiField) {
                Comment comment = apiField.field.getAnnotation(Comment.class);
                if(null!=comment){
                    apiField.description = comment.value();
                }
            }
        };
        return quickAPIOption;
    }
}
