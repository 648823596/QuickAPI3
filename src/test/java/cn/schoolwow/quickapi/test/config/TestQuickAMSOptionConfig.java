package cn.schoolwow.quickapi.test.config;

import cn.schoolwow.ams.domain.block.list.common.AMSFieldOption;
import cn.schoolwow.ams.domain.option.AMSFieldListener;
import cn.schoolwow.ams.domain.option.QuickAMSOption;
import cn.schoolwow.quickdao.annotation.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestQuickAMSOptionConfig {
    @Bean
    @Autowired
    public QuickAMSOption quickAMSOption(ApplicationContext applicationContext){
        QuickAMSOption quickAMSOption = new QuickAMSOption();
        quickAMSOption.debug = true;
        quickAMSOption.amsFieldListener = new AMSFieldListener() {
            @Override
            public void handleAMSField(AMSFieldOption amsFieldOption) {
                if(null!=amsFieldOption.entityField){
                    Comment comment = amsFieldOption.entityField.getAnnotation(Comment.class);
                    if(null!=comment){
                        amsFieldOption.label = comment.value();
                    }
                }
                if(null!=amsFieldOption.property&&null!=amsFieldOption.property.field){
                    Comment comment = amsFieldOption.property.field.getAnnotation(Comment.class);
                    if(null!=comment){
                        amsFieldOption.label = comment.value();
                    }
                }
            }
        };
        quickAMSOption.packageNameLabelMap.put("cn.schoolwow.quickapi.test.module.system", "系统");
        quickAMSOption.packageNameLabelMap.put("cn.schoolwow.quickapi.test.module.user", "用户管理");
        return quickAMSOption;
    }
}
