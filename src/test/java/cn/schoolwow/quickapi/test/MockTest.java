package cn.schoolwow.quickapi.test;

import cn.schoolwow.quickapi.domain.QuickAPIOption;
import cn.schoolwow.quickapi.module.common.flow.MockDataFlow;
import cn.schoolwow.quickapi.test.module.mock.MockJavaType;
import cn.schoolwow.quickapi.test.module.system.domain.UserLoginInfo;
import cn.schoolwow.quickapi.test.module.user.domain.DeleteUserRequest;
import cn.schoolwow.quickflow.QuickFlowBuilder;
import cn.schoolwow.quickflow.domain.FlowContext;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class MockTest {
    @Test
    public void mockTest(){
        QuickAPIOption quickAPIOption = new QuickAPIOption();
        quickAPIOption.debug = true;
        quickAPIOption.controllerPackageNameList = Arrays.asList(
                "cn.schoolwow.quickapi.test.module"
        );
        quickAPIOption.entityPackageNameList = Arrays.asList(
                "cn.schoolwow.quickapi.test.module",
                "cn.schoolwow.quickdao.domain"
        );
        FlowContext flowContext = QuickFlowBuilder.startFlow(new MockDataFlow())
                .putInstanceData(quickAPIOption)
                .putCurrentCompositeFlowData("clazz", UserLoginInfo.class)
                .putCurrentCompositeFlowData("printException", true)
                .putTemporaryData("methodName", "无")
                .execute();
        String jsonString = flowContext.checkData("jsonString",String.class);
        System.out.println(jsonString);
    }
}
