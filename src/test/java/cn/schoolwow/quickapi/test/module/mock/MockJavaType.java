package cn.schoolwow.quickapi.test.module.mock;

import cn.schoolwow.quickapi.test.module.user.domain.User;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class MockJavaType {
    public long userId;

    public List<Long> idList;

    public Set<Long> idSet;

    public Map<Long, User> userIdMap;
}
