package cn.schoolwow.quickapi.test.module.system.controller;

import cn.schoolwow.quickapi.test.module.system.domain.UserLoginInfo;
import cn.schoolwow.quickapi.test.module.system.domain.UserLoginRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统控制器
 * */
@RestController
@RequestMapping("/system")
public class SystemController {
    /**
     * 登录
     * @param userLoginRequest 用户登录请求
     * */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public UserLoginInfo login(
            @RequestBody UserLoginRequest userLoginRequest
    ){
        UserLoginInfo userLoginInfo = new UserLoginInfo();
        return userLoginInfo;
    }

}
