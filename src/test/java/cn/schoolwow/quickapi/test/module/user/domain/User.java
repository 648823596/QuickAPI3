package cn.schoolwow.quickapi.test.module.user.domain;

import cn.schoolwow.quickdao.annotation.*;

import java.time.LocalDateTime;


@Comment("用户")
@UniqueField(columns = "number")
public class User {
    @Id
    private long id;

    @Comment("用户名")
    @Constraint(notNull = true)
    @Index(indexType = IndexType.UNIQUE)
    private String username;

    @Comment("密码")
    @Constraint(notNull = true)
    private String password;

    @Comment("工号")
    @Index(indexType = IndexType.UNIQUE)
    private String number;

    @Comment("邮箱")
    @Constraint(notNull = true)
    @Index(indexType = IndexType.UNIQUE)
    private String email;

    @Comment("所属部门")
    private Long departmentId;

    @TableField(createdAt = true)
    private LocalDateTime createdAt;

    @TableField(updatedAt = true)
    private LocalDateTime updatedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
