package cn.schoolwow.quickapi.test.module.attachment;

import cn.schoolwow.quickapi.test.module.user.domain.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 上传附件
 * */
@RestController
@RequestMapping("/attachment")
public class UploadFileController {
    /**
     * 上传文件
     * @param projectId 项目id
     * @param file 文件对象
     * */
    @PostMapping("/uploadFile")
    public boolean checkExits(
            @RequestPart("projectId") Long projectId,
            @RequestPart("file") MultipartFile file
    ) {
        return false;
    }

    /**
     * 创建台账
     * @param file 文件对象
     * @param user 创建台账用户
     * */
    @PostMapping("/createStandingBook")
    public void createStandingBook(
            @RequestPart("file") MultipartFile file,
            @RequestPart("user") User user
    ) {
    }

}
