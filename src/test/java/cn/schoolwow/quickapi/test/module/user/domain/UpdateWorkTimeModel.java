package cn.schoolwow.quickapi.test.module.user.domain;

import com.alibaba.fastjson.JSONObject;

public class UpdateWorkTimeModel {
    /**模型id*/
    public Long id;

    /**模型名称*/
    public String name;

    /**模型版本*/
    public String version;

    /**适用项目模板*/
    public Long projectTemplateId;

    /**模型级别(1:中心,2:处所,3:领域,4:WBS工作项)*/
    public Integer level;

    /**上级模型*/
    public Long parentId;

    /**用户*/
    public User user;

    /**时间节点耗费资源*/
    public JSONObject timeNodeWorkHour;

}
