package cn.schoolwow.quickapi.test.module.attachment;

import cn.schoolwow.ams.domain.response.AMSResult;
import cn.schoolwow.quickflow.QuickFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 上传附件
 * */
@RestController
@RequestMapping("/attachment/upload")
public class AttachmentUploadController {
    /**
     * 检查是否存在
     * @param fileName 文件名
     * @param md5 分块文件md5值
     * @param fileSuffix 文件后缀
     * */
    @GetMapping("/checkExits")
    public boolean checkExits(
            @RequestParam String fileName,
            @RequestParam String md5,
            @RequestParam String fileSuffix
    ) {
        return false;
    }

    /**
     * 上传分块
     * @param md5 分块文件md5值
     * @param fileName 文件名
     * @param fileSuffix 文件后缀
     * @param chunkIndex 分块序号
     * @param chunk 分块
     * */
    @PostMapping("/upload")
    public void upload(
            @RequestParam String md5,
            @RequestParam String fileName,
            @RequestParam String fileSuffix,
            @RequestParam Integer chunkIndex,
            @RequestParam Integer chunkTotal,
            @RequestParam MultipartFile chunk
    ) throws IOException {
    }

    /**
     * 合并分块
     * @param md5 分块文件md5值
     * @param chunkTotal 分块总个数
     * @param fileSuffix 文件后缀
     * */
    @PostMapping("/merge")
    public AMSResult merge(
            @RequestParam String fileName,
            @RequestParam String md5,
            @RequestParam String fileSuffix,
            @RequestParam Integer chunkTotal
    ) {
        return null;
    }
}
