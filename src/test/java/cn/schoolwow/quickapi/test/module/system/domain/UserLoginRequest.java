package cn.schoolwow.quickapi.test.module.system.domain;

import cn.schoolwow.quickdao.annotation.Comment;

@Comment("用户登录请求")
public class UserLoginRequest {
    @Comment("用户名")
    public String username;

    @Comment("密码")
    public String password;
}
