package cn.schoolwow.quickapi.test.module.system.domain;

/**用户登录信息*/
public class UserLoginInfo {
    /**用户id*/
    public long userId;

    /**用户名*/
    public String username;

    /**用户邮箱*/
    public String email;

    /**角色id*/
    public Long roleId;

    /**角色key*/
    public String roleKey;

    /**角色名*/
    public String roleName;

    /**所属部门*/
    public Long departmentId;

    /**鉴权*/
    public String token;

    /**角色id列表*/
    public Long[] roleIdArray;

    /**角色名称列表*/
    public String[] roleNameArray;
}
