package cn.schoolwow.quickapi.test.module.user.controller;

import cn.schoolwow.quickapi.test.module.user.domain.DeleteUserRequest;
import cn.schoolwow.quickapi.test.module.user.domain.User;
import cn.schoolwow.quickapi.test.module.system.domain.UserLoginInfo;
import cn.schoolwow.quickdao.domain.database.dql.response.PageVo;
import org.springframework.web.bind.annotation.*;

/**
 * 用户列表查询
 * */
@RestController
@RequestMapping("/user")
public class UserQueryController {
    /**
     * 查询用户详情
     * @param userId 用户id
     * @return 用户登录信息
     * */
    @RequestMapping(value = "/getUserDetail/{userId}", method = RequestMethod.GET)
    public UserLoginInfo getUserDetail(
            @PathVariable("userId") Long userId
    ){
        UserLoginInfo userLoginInfo = new UserLoginInfo();
        return userLoginInfo;
    }

    /**
     * 查询用户列表
     * */
    @RequestMapping(value = "/listUser", method = RequestMethod.GET)
    public PageVo<User> listUser(
    ){
        return null;
    }

    /**
     * 根据用户类型查询
     * @param type 用户类型
     * */
    @RequestMapping(value = "/queryUserByType", method = RequestMethod.GET)
    public PageVo<User> queryUserByType(
            @RequestParam("type") String type
    ){
        return null;
    }

    /**
     * 删除用户
     * @param deleteUserRequest 删除用户类型
     * */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public void delete(
            @RequestBody DeleteUserRequest deleteUserRequest
    ){
    }
}
